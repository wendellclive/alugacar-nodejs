import { SpecificationRepository } from "../../repositories/implementations/SpecificationsRepositry"
import { CreateSpecificationUseCase } from "./CreateSpecificationUseCase"
import { CreateSpecificationsController } from "./CreateSpecificationsController";

const specificationsRepository = new SpecificationRepository()
const createSpecificationUseCase = new CreateSpecificationUseCase(specificationsRepository)
const createSpecificationController = new CreateSpecificationsController(createSpecificationUseCase)

export { createSpecificationController }