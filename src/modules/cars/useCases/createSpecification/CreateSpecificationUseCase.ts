import { ISpecificationRepository } from "../../repositories/ISpecificationsRepositry";

interface IRequest {
    name: string;
    description: string
}

class CreateSpecificationUseCase {

    constructor(private specificationsRepository: ISpecificationRepository) {

    }

    execute({name, description}: IRequest) {

        const specificationAlreadyExists = this.specificationsRepository.findByName(name)

        if(specificationAlreadyExists) {
            throw new Error('Specification alredy exists!!')
        }
        
        this.specificationsRepository.create({
            name,
            description
        })
        
    }

}

export { CreateSpecificationUseCase }