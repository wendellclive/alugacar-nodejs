import { ICreateCategoriesRepository } from "../../repositories/ICategoriesRepository";

interface IRequest {
    name: string;
    description: string;
}

/**
 * - Definir o tipo de retorno - OK
 * - Alterar o retorno de erro - OK
 * - Acessar o repositorio     - OK
 */

class CreateCategoryUseCase {

    constructor(private categoriesRepository: ICreateCategoriesRepository) {   }

    execute({ description, name}: IRequest) : void {

        const categoryAlreadyExists = this.categoriesRepository.findByName(name)

        if (categoryAlreadyExists) {
            throw new Error('Category Already exists!!')
        }

        this.categoriesRepository.create({name, description})

    }

}

export { CreateCategoryUseCase }