import { Category } from "../../entities/Category";
import { ICreateCategoriesRepository } from "../../repositories/ICategoriesRepository"

class ListCategoriesUseCase {

    constructor(private categoriesRepository: ICreateCategoriesRepository) { }

    execute(): Category[] {

        const categories = this.categoriesRepository.list()
        return categories;

    }
}

export { ListCategoriesUseCase }