import { Category } from "../entities/Category";

interface ICreateCategoryDTO {
    name: string;
    description: string
}

interface ICreateCategoriesRepository {

    findByName(Name: string): Promise<Category>;
    list(): Promise<Category[]>;
    create({ name, description }: ICreateCategoryDTO): void;

}

export { ICreateCategoriesRepository, ICreateCategoryDTO }